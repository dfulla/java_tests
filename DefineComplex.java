class DefineComplex {

    public double re;
    public double im;

    public DefineComplex(double r,double i){
    re = r;
    im = i;
    //System.out.println("created complex number "+toStr());
    
    }

    public String toStr(){
    return String.valueOf(re)+"+"+String.valueOf(im)+"*i";
    
    }

    public static void sumComplex(DefineComplex c3, DefineComplex c4){
    //System.out.println("This is display");
    //System.out.println(c3.toStr());

    double sumRe = c3.re + c4.re ;
    double sumIm = c3.im + c4.im;

    DefineComplex x1 = new DefineComplex(sumRe, sumIm);

    System.out.println(String.valueOf(x1.re)+"+"+String.valueOf(x1.im)+"*i");

    //return String x1;

    }

    public static void subComplex(DefineComplex c3, DefineComplex c4){
    //System.out.println("This is display");
    //System.out.println(c3.toStr());

    double subRe = c3.re - c4.re ;
    double subIm = c3.im - c4.im;

    DefineComplex x1 = new DefineComplex(subRe, subIm);

    System.out.println(String.valueOf(x1.re)+"+"+String.valueOf(x1.im)+"*i");

    //return String x1;

    }

    public static void main(String[] args){
        
    DefineComplex t7 = new DefineComplex(7.3,2.5);
    System.out.println("created complex number "+t7.toStr());
    
    sumComplex(t7,t7);
    subComplex(t7, t7);


    }


}