import java.util.Scanner;

class InputComplex {

    public double re;
    public double im;

    public InputComplex(double r,double i){
    re = r;
    im = i;
    //System.out.println("created complex number "+toStr());
    
    }

    public String toStr(){
    return String.valueOf(re)+"+"+String.valueOf(im)+"*i";
    
    }

    public static void sumComplex(InputComplex c3, InputComplex c4){
    //System.out.println("This is display");
    //System.out.println(c3.toStr());

    double sumRe = c3.re + c4.re ;
    double sumIm = c3.im + c4.im;

    InputComplex x1 = new InputComplex(sumRe, sumIm);

    System.out.println(String.valueOf(x1.re)+"+"+String.valueOf(x1.im)+"*i");

    //return String x1;

    }

    public static void subComplex(InputComplex c3, InputComplex c4){
    //System.out.println("This is display");
    //System.out.println(c3.toStr());

    double subRe = c3.re - c4.re ;
    double subIm = c3.im - c4.im;

    InputComplex x1 = new InputComplex(subRe, subIm);

    System.out.println(String.valueOf(x1.re)+"+"+String.valueOf(x1.im)+"*i");

    //return String x1;

    }

    public static void main(String[] args){
        
    Scanner real1_input = new Scanner(System.in);
    System.out.println("Input real part:");
    double real1 = real1_input.nextDouble();

    Scanner imag1_input = new Scanner(System.in);
    System.out.println("Input imaginary part part:");
    double imag1 = imag1_input.nextDouble();

    //DefineComplex t7 = new DefineComplex(7.3,2.5);

    InputComplex t7 = new InputComplex(real1,imag1);

    System.out.println("created complex number "+t7.toStr());
    
    sumComplex(t7,t7);
    subComplex(t7, t7);


    }


}
